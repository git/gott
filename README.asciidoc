= gott
apiote <me@apiote.xyz>
v2.0.3 2022-04-12
:toc:

gott is a Railway Oriented Programming library for Go.

In ROP a program is a chain of functions wrapped in blocks resembling track switches. It’s a simplification of an Either monad.

gott provides N types of blocks:

* Bind, which wraps a function that continues on the happy track or switches to the sad track

    >------+-------->
            \
             \
    >---------+----->

* Map, which wraps a function that will alwayc continue on the happy track

    >--------------->
    >--------------->

* Tee, which wraps a function performing side effects and can switch to the sad track

             _
             |
    >--------++----->
               \
                \
    >------------+-->

* SafeTee, which is to Tee what Map is to Bind

             _
             |
    >--------+------>
    >--------------->

* Recover, which wraps a function that tries to return to the happy Path

    >--------+------>
            /
           /
    >-----+--------->

* Catch, which switches to the sad track in case of a panic
* Handle, which does different things depending on which track the processing is

== Usage

Provided functions are methods of `R[T any]` generic and return `R[T]` so that they can be chained.

Usage can be seen in tests, the simplest being

    import (
    	"apiote.xyz/p/gott/v2"
    )
    func divide5(by int) (int, error) {
    	if by == 0 {
    		return by, errors.New("divideByZero")
    	} else {
    		return 5 / by, nil
    	}
    }
    func main() {
    	r := R[int]{S: 5}.Bind(divide5)
    	// r.S == 1; r.E == nil
    }

== Contribute

This project is finished; no more functions will be implemented; all feature requests will be ignored.

This project uses The Code of Merit, which is available as CODE_OF_CONDUCT file.

Fixes and patches are welcome; please send them to `gott@git.apiote.xyz` using `git send-email`. They must include a sign-off to certify agreement to https://developercertificate.org/[Developer Certificate of Origin].

All communication—questions, bugs, etc.—should go through the mailing list available at `gott@git.apiote.xyz`. Note that all communication will be made public at https://asgard.apiote.xyz/.

== Mirrors

The canonical repository for this project is https://git.apiote.xyz/gott.git it’s mirrored at https://notabug.org/apiote/gott

Mirrors exist solely for the sake of the code and any additional functions provided by third-party services (including but not limited to issues and pull requests) will not be used and will be ignored.

== License

----
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
----
